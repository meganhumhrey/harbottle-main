# tomcat8

[Apache Tomcat 8](https://tomcat.apache.org/download-80.cgi) package in the
[harbottle-main](https://harbottle.gitlab.io/harbottle-main) CentOS repo.

## Suggested Basic Installation Procedure for CentOS 8

The following script installs the latest version of Apache Tomcat 8. It then
enables and starts the `tomcat8` service.

After running this script, Tomcat is available at http://localhost:8080

```bash
#!/bin/bash

# Install Apache Tomcat 8 using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install tomcat8

# Recommended: install native library
dnf -y install tomcat8-native

# Optional: install webapps (uncomment to install)
#dnf -y install tomcat8-admin-webapps
#dnf -y install tomcat8-docs-webapp
#dnf -y install tomcat8-webapps

# Enable and start Tomcat 8
systemctl enable --now tomcat8
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs the latest version of Apache Tomcat 8. It then
enables and starts the `tomcat8` service.

After running this script, Tomcat is available at http://localhost:8080

```bash
#!/bin/bash

# Install Apache Tomcat 8 using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install tomcat8

# Recommended: install native library
yum -y install tomcat8-native

# Optional: install webapps (uncomment to install)
#yum -y install tomcat8-admin-webapps
#yum -y install tomcat8-docs-webapp
#yum -y install tomcat8-webapps

# Enable and start Tomcat 8
systemctl enable --now tomcat8
```
