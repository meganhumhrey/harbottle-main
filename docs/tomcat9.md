# tomcat9

[Apache Tomcat 9](https://tomcat.apache.org/download-90.cgi) package in the
[harbottle-main](https://harbottle.gitlab.io/harbottle-main) CentOS repo.

## Suggested Basic Installation Procedure for CentOS 8

The following script installs the latest version of Apache Tomcat 9. It then
enables and starts the `tomcat9` service.

After running this script, Tomcat is available at http://localhost:8080

```bash
#!/bin/bash

# Install Apache Tomcat 9 using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install tomcat9

# Recommended: install native library
dnf -y install tomcat9-native

# Optional: install webapps (uncomment to install)
#dnf -y install tomcat9-admin-webapps
#dnf -y install tomcat9-docs-webapp
#dnf -y install tomcat9-webapps

# Enable and start Tomcat 9
systemctl enable --now tomcat9
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs the latest version of Apache Tomcat 9. It then
enables and starts the `tomcat9` service.

After running this script, Tomcat is available at http://localhost:8080

```bash
#!/bin/bash

# Install Apache Tomcat 9 using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install tomcat9

# Recommended: install native library
yum -y install tomcat9-native

# Optional: install webapps (uncomment to install)
#yum -y install tomcat9-admin-webapps
#yum -y install tomcat9-docs-webapp
#yum -y install tomcat9-webapps

# Enable and start Tomcat 9
systemctl enable --now tomcat9
```
