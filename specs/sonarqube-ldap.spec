%define __jar_repack 0

Name:     sonarqube-ldap
Version:  2.2.0.608
Release:  8%{?dist}.harbottle
Summary:  SonarQube LDAP plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-ldap-plugin/sonar-ldap-plugin-%{version}.jar
Autoprov: no

%description
LDAP plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-ldap-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-ldap-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 2.2.0.608-8
  - Spec file changes for el8

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.2.0.608-7
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.2.0.608-6
  - Standardize SonarQube plugins

* Mon Oct 15 2018 grainger@gmail.com - 2.2.0.608-5
  - Upate download source

* Sat Mar 03 2018 grainger@gmail.com - 2.2.0.608-5
  - Update iteration

* Tue Oct 10 2017 grainger@gmail.com - 2.2.0.608-3
  - Fix dir

* Tue Oct 10 2017 grainger@gmail.com - 2.2.0.608-2
  - Fix files

* Tue Oct 10 2017 grainger@gmail.com - 2.2.0.608-1
  - Initial packaging
