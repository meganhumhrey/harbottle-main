%define __jar_repack 0

Name:     sonarqube-auth-bitbucket
Version:  1.1.0.381
Release:  2%{?dist}.harbottle
Summary:  SonarQube Bitbucket authentication plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/SonarSource/sonar-auth-bitbucket
Source0:  https://binaries.sonarsource.com/Distribution/sonar-auth-bitbucket-plugin/sonar-auth-bitbucket-plugin-%{version}.jar
Autoprov: no

%description
Bitbucket authentication plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-auth-bitbucket-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-bitbucket-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-bitbucket-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 1.1.0.381-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.1.0.381-1
  - Initial packaging
