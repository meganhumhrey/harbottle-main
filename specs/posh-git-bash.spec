%global appname posh-git-bash
%global shortappname posh-git-sh
%global binname posh-git
%global appvendor lyze

Name:           %{appname}
Version:        1.3.0
Release:        1%{?dist}.harbottle
Summary:        Bash version of the posh-git command prompt

License:        MIT
URL:            https://github.com/%{appvendor}/%{shortappname}
Source0:        https://github.com/%{appvendor}/%{shortappname}/archive/v%{version}.tar.gz
Source1:        %{binname}.sh

Requires:       bash
Requires:       bash-completion
Requires:       git

%description
This script allows you to see the status of the current git repository in your
prompt. It replicates the prompt status from the Windows PowerShell module
dahlbyk/posh-git (https://github.com/dahlbyk/posh-git).

%prep
%setup -qn %{shortappname}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -m 755 git-prompt.sh $RPM_BUILD_ROOT%{_bindir}/%{binname}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/%{binname}.sh

%files
%doc README.md
%{_bindir}/%{binname}
%{_sysconfdir}/profile.d/%{binname}.sh

%changelog
* Tue Jun 23 2020 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Wed Apr 03 2019 - harbottle@room3d3.com - 1.2.0-1
  - Bump version

* Sat Jan 26 2019 - harbottle@room3d3.com - 1.1.1-3
  - Add harbottle to release

* Wed Aug 23 2017 <grainger@gmail.com>
- Initial packaging
