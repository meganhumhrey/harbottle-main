Name:     bower
Version:  1.8.12
Release:  1%{?dist}.harbottle
Summary:  A package manager for the web
Group:    Applications/System
License:  MIT
Url:      https://%{modname}.io
Requires: nodejs-bower = %{version}

%description
Verdaccio is a lightweight private npm proxy registry built in Node.js.

%prep

%build

%files

%changelog
* Mon Jan 18 2021 - harbottle@room3d3.com - 1.8.12-1
  - Bump version

* Thu Jan 14 2021 - harbottle@room3d3.com - 1.8.10-1
  - Bump version

* Sat Jan 04 2020 - harbottle@room3d3.com - 1.8.8-2
  - Initial package
