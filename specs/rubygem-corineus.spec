%global gem_name corineus

Name:          rubygem-%{gem_name}
Version:       0.1.2
Release:       2%{?dist}.harbottle
Summary:       Authenticated updates of DNS records on a MS Windows DNS server
Group:         Applications/System
License:       MIT
URL:           https://gitlab.com/harbottle/corineus
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires:      bind-utils
Requires:      krb5-workstation
Requires:      ruby(release)
Requires:      ruby(rubygems)
Requires:      ruby
Requires:      rubygem(colorize)
Requires:      rubygem(POpen4)
Requires:      rubygem(thor)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildArch:     noarch

%description
Corineus is a wrapper for the kinit and nsupdate commands to allow easy
authenticated updates of DNS records on a Microsoft Windows DNS server from
Linux.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install
%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%files
%exclude %{gem_instdir}/.*
%exclude %{gem_instdir}/{Gemfile,Rakefile}
%doc %{gem_instdir}/{README.md,%{gem_name}.gemspec}
%license %{gem_instdir}/LICENSE.txt
%dir %{gem_instdir}
%{gem_libdir}
%{gem_instdir}/bin
%{_bindir}/%{gem_name}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/%{gem_name}.gemspec

%changelog
* Sat Jan 04 2020 - harbottle@room3d3.com - 0.1.2-2
  - Improve dependencies

* Sat Dec 21 2019 - harbottle@room3d3.com - 0.1.2-1
  - Initial packaging
